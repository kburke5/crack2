#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

//Read Num of Strs in dictonary
int NumofStrs(char* filename)
{
    FILE *NumLines;
    NumLines = fopen(filename, "r");
    if(!filename)
    {
        printf("Couldn't open %s\n", filename);
        exit(1);
    }
    int c, lines =0;
    while((c = fgetc(NumLines))!= EOF)
    {
        if(c =='\n')
        {
            lines++;
        }
    }
    return lines;
}

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash. 
// That is, return 1 if the guess is correct.
//char *md5(const char *str, int length) 
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *hashGuess = md5(guess, strlen(guess));
    // Compare the two hashe
    if(strcmp(hashGuess, hash ) == 0)
    {
        //printf("The hashes match!\n");
        return 1;
    }
    else
    {
        return 0;   
    }
}


// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename, int len)
{
    FILE *hasInp;
    hasInp = fopen(filename, "r");
    if( !filename )
    {
        printf("Couldn't open %s\n", filename);
        exit(1);
    }
    //SPC for Array of char pointers
    char** array = malloc(len*sizeof(char*));
    //create holder string & scan dict file
    int i = 0;
    char lineBuffer[HASH_LEN+1];
    while (fgets(lineBuffer,sizeof(lineBuffer), hasInp) != NULL)
    {
        //gets rid of NewLine Char
        char *ptr = strtok(lineBuffer,"\n");
        //alloc spc for str
        array[i] = malloc((strlen(lineBuffer)+1) * sizeof(char));
        //copy str into array
        strcpy(array[i],ptr);
        i++;
       
        
    }
    return array;
}


int main(int argc, char *argv[])
{
    if (argc < 2) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    // Read the hash file into an array of strings
    int hlen = NumofStrs(argv[1]);
    char **hashes = readfile(argv[1],hlen);

    // Read the dictionary file into an array of strings
    int dlen = NumofStrs(argv[2]);
    char **dict = readfile(argv[2],dlen);

    // For each hash, try every entry in the dictionary.
    int z =0;
    while(hashes[z]!=NULL)
    {
        for(int i =0;dict[i]!=NULL;i++)
        {
            if(tryguess(hashes[z],dict[i])==1)
            {
                printf("Dictonary word: %s equals the Hash: %s\n",dict[i],hashes[z]);
                break;
            }
        }
        z++;
    }
    // Print the matching dictionary entry.
    // Need two nested loops.
}
